package com.sample.bootfaces.persistence;

import com.sample.bootfaces.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
